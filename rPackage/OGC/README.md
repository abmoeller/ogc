OGC (Oblique geographic coordinates)
====================================

The goal of OGC is to create a stack of rasterlayers containing the
coordinates of the cells along a series of axes tilted at oblique angles
relative to the x-axis.

These coordinate rasters can be used as covariates for machine learning
models, in order to incorporate spatial relationships in the prediction.

For questions about the package, please contact Anders Bjørn Møller
([email](mailto:anbm@agro.au.dk)).

Installation
------------

Make sure the devtools package is installed first:

    install.packages('devtools')

    library(devtools)

    install_bitbucket('abmoeller/ogc/rPackage/OGC')

Example
-------

This example shows how to generate rasters with coordinates tilted at
six different angles for the Vindum dataset:

    library(OGC)
    library(raster)

    ## Loading required package: sp

    data('Vindum_covariates')
    ogcs <- makeOGC(Vindum_covariates[[1]], 6)
    plot(ogcs, legend = FALSE)

![](README_files/figure-markdown_strict/example1-1.png)

The oblique geographic coordinates can be used as covariates for spatial
predictions. This example shows how to use them for prediction the
amounts of soil organic matter (SOM) in the Vindum field, using Random
Forest models trained using the package `ranger`. If ranger is not
installed, first run:

    install.packages('ranger', dependencies = TRUE)

Then run the example:

    # Load ranger

    library(ranger)


    # Load data and generate OGCs, and extract them to the observations

    data('Vindum_SOM')

    ogcs <- makeOGC(Vindum_covariates[[1]], 8)

    pts1 <- extract(ogcs, Vindum_SOM, sp = TRUE)


    # Create formula for the model

    fm1 <- as.formula(paste0('SOM ~ ', paste(names(ogcs), collapse = ' + ')))


    # Train the model

    RF_OGC_1 <- ranger(fm1, data = pts1@data, mtry = 8, splitrule = 'extratrees', importance = 'impurity', min.node.size = 1, keep.inbag = TRUE)


    # Predict SOM contents and show the results

    prediction_1 <- predict(ogcs, RF_OGC_1, fun = function(model, ...) predict(model, ...)$predictions)

    plot(prediction_1)

![](README_files/figure-markdown_strict/example2-1.png)

The oblique geographic coordinates can also be used in combination with
the existing covariates for the field:

    # Stack rasters with covariates and OGCs

    rstack <- stack(ogcs, Vindum_covariates)


    # Predict SOM as in the previous example

    pts2 <- extract(rstack, Vindum_SOM, sp = TRUE)

    fm2 <- as.formula(paste0('SOM ~ ', paste(names(rstack), collapse = ' + ')))

    RF_OGC_2 <- ranger(fm2, data = pts2@data, mtry = 8, splitrule = 'extratrees', importance = 'impurity', min.node.size = 1)

    prediction_2 <- predict(rstack, RF_OGC_2, fun = function(model, ...) predict(model, ...)$predictions)

    plot(prediction_2)

![](README_files/figure-markdown_strict/example3-1.png)

The importance of the coordinate rasters can be plotted intuitively in
the same manner as a wind rose. This example requires the packages
`ggplot2` and `gridExtra`. If these two packages are not installed,
first run:

    install.packages(c('ggplot2', 'gridExtra'), dependencies = TRUE)

Then run the example:

    # Load packages

    library(ggplot2)
    library(gridExtra)


    # Put covariate importance for the two Random Forest models into a list

    imp <- list()

    imp[[1]] <- data.frame(Overall = importance(RF_OGC_1))
    imp[[2]] <- data.frame(Overall = importance(RF_OGC_2)[1:8])

    imp <- lapply(imp, function(x) {
      x <- rbind(x, x)
      x$dir <- c(1:nrow(x))
      return(x)})


    # Plot covariate importance for OGC in the two models

    brks <- seq(from = min(imp[[1]]$dir), by = (max(imp[[1]]$dir) + 1 - min(imp[[1]]$dir))/4, length.out = 4)

    titles <- c('A: Eight angles', 'B: Eight angles + auxiliary data')

    fig <- list()

    for(i in 1:2)
    {
      fig[[i]] <- ggplot(imp[[i]], aes(x = dir, y = Overall)) +
        coord_polar(start = -pi/2 - pi/(nrow(imp[[i]])), direction = -1) +
        geom_col(width = 1, colour = 'black', fill = rgb(0,2/3,2/3,1/2)) +
        ggtitle(titles[[i]]) +
        scale_x_continuous(breaks = brks
                           , labels = c('E', 'N', 'W', 'S')
        ) +
        ylab('Importance (variance)') +
        theme_bw() +
        theme(axis.text.x = element_text(colour = 'black')
              , axis.title.x = element_blank()
              , axis.text.y = element_text(colour = 'black')
              , panel.grid.major  = element_line(color = 'grey')
              , panel.grid.minor  = element_blank()
              , panel.border = element_rect(size = 1)
        )
    }

    grid.arrange(fig[[1]], fig[[2]], nrow = 1)

![](README_files/figure-markdown_strict/example4-1.png)

The sizes of the bars show the importance of the coordinates tilted at
the angle of the bar. The bars are repeated for opposite angles, as the
importance is directionless.

Citation
--------

If you use the contents of the package, please refer to:

Møller, A. B., Beucher, A. M., Pouladi, N., and Greve, M. H.: Oblique
geographic coordinates as covariates for digital soil mapping, SOIL
Discuss., <https://doi.org/10.5194/soil-2019-83>, in review, 2019.

News
----

### 2019-11-13: Version 1.0.1

-   Fixed bug in `Vindum_covariates`.
-   Updated citation section in the readme.
